type HeaderProps = {
  headerText: string;
};

// functional component #1
const Header = (props: HeaderProps) => {
  return (
    <div>
      <h1>{props.headerText}</h1>
    </div>
  );
};

export default Header;