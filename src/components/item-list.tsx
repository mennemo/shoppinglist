import { useEffect, useState } from "react";
import Item from "../models/item";
import ShoppinglistApiService from "../services/shoppinglist-api-service";
import ItemFilter from "./item-filter";
import ItemForm from "./item-form";
import ItemItem from "./item-item";

const ItemList = () => {
  const apiService = new ShoppinglistApiService();

  // state Hook
  const [items, setItems] = useState<Item[]>([]);
  const [currentFilter, setCurrentFilter] = useState<string>("all");

  const fetchItems = async () => {
    const data = await apiService.getItems(currentFilter);
    setItems(data);
  };

  useEffect(() => {
    fetchItems();
  }, [currentFilter]);

  const handleComplete = (item: Item) => {
    item.done = !item.done;
    setItems(items.map((element) => (element.id === item.id ? item : element)));
    apiService.updateItem(item);
  };

  const handleRemove = (id: number) => {
    setItems(items.filter((element) => element.id !== id));
    apiService.deleteItem(id);
  };

  const handleAdd = async (item: Item) => {
    const createdTodo = await apiService.createItem(item);
    setItems([createdTodo, ...items]);
  };

  const handleFilterChange = (filterOption: string) => {
    console.log("Filtering: ", filterOption);

    // filter list
    setCurrentFilter(filterOption);
  };

  return (
    <div className="div_form_and_itemlist">
      <div className="div_form_and_filter">
        <ItemForm onAdd={handleAdd} />
        <ItemFilter onFilterChange={handleFilterChange} />
      </div>
      {/* checks if list is empty or not */}
      {items && items.length > 0 ? (
        <>
          {items.map((element) => {
            return (
              <div className="div_itemlist" key={element.id}>
                <ItemItem
                  item={element}
                  onComplete={handleComplete}
                  onRemove={handleRemove}
                />
              </div>
            );
          })}
        </>
      ) : (
        <>List is empty - add your first items!</>
      )}
    </div>
  );
};

export default ItemList;
