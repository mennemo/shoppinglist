import { useState } from "react";
import Item from "../models/item";

type ItemItemProps = {
  item: Item;
  onComplete: Function;
  onRemove: Function;
};

const ItemItem = (props: ItemItemProps) => {
  const [isActive, setActive] = useState(false);

  const toggleClass = () => {
    setActive(!isActive);
  };

  function onClick() {
    toggleClass();
    props.onComplete(props.item);
  }

  const descriptionAdded = props.item.description;

  return (
    <div
      className={props.item.done ? "div_item_complete" : "div_item_incomplete"}
    >
      <h2 className="item_title">{props.item.title}</h2>
      <div className="item_description">{descriptionAdded ? <p>{props.item.description}</p> : ""}</div>
      <p>
        {props.item.amount} {props.item.unit}
      </p>
      <p>Item state - {props.item.done ? "✔" : <span className="item_not_added">not added yet</span>}</p>
      <div className="div_item_buttons">
        <button
          className={
            props.item.done ? "btn_complete btn_incomplete" : "btn_complete"
          }
          onClick={onClick}
        >
          {props.item.done ? "add again" : "✔"}
        </button>

        <button
          className="btn_delete"
          onClick={() => props.onRemove(props.item.id)}
        >
          🗑
        </button>
      </div>
    </div>
  );
};

export default ItemItem;
