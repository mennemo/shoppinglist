import { useEffect, useState } from "react";

type ItemFilterProps = {
  onFilterChange: Function;
};

const ItemFilter = (props: ItemFilterProps) => {

  const [activeFilter, setActiveFilter] = useState<string>("all");

  const filter = (event: any) => {
    const btnId = event.target.id;

    if (btnId === "btn_filter_nay") {
      setActiveFilter("nay");
      props.onFilterChange("nay");
      // add class (btn_active) to button, but remove when changed
    } else if (btnId === "btn_filter_yay") {
      props.onFilterChange("yay");
      setActiveFilter("yay");
      // add class (btn_active) to button, but remove when changed
    } else {
      props.onFilterChange("all");
      setActiveFilter("all");
      // add class (btn_active) to button, but remove when changed
    }
  };


  return (
    <div className="div_filters">
      <p className="title_filters">Filter:</p>
      <div className="div_btn_filters">
        <button onClick={filter} id="btn_filter_all" className={activeFilter === "all" ? "btn_filter btn_active" : "btn_filter"}>
          Show All
        </button>
        <button onClick={filter} id="btn_filter_nay" className={activeFilter === "nay" ? "btn_filter btn_active" : "btn_filter"}>
          Not added yet
        </button>
        <button onClick={filter} id="btn_filter_yay" className={activeFilter === "yay" ? "btn_filter btn_active" : "btn_filter"}>
          Added
        </button>
      </div>
    </div>
  );
};

export default ItemFilter;
