import React, { useState } from "react";
import Item from "../models/item";

type ItemFormProps = {
  onAdd: Function;
};

const ItemForm = (props: ItemFormProps) => {
  const [formItem, setFormItem] = useState<Item>(new Item("", 0));

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const target = e.target as HTMLInputElement;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const key = target.id;

    setFormItem({ ...formItem, [key]: value });
  };

  return (
    <div className="div_input_form">
      <h3>Add an item to your shopping list:</h3>
      <label htmlFor="title">Item (required):</label>
      <br />
      <input
        type="text"
        id="title"
        className="input_field"
        value={formItem.title}
        onChange={(e) => handleInputChange(e)}
        placeholder="Name of the item"
      />
      <br />
      <label htmlFor="description">Description:</label>
      <br />
      <textarea
        id="description"
        className="input_field"
        value={formItem.description}
        onChange={(e) => handleInputChange(e)}
        placeholder="Description of the item"
      ></textarea>
      <br />
      <label htmlFor="amount">Amount (required):</label>
      <br />
      <input
        type="number"
        id="amount"
        className="input_field"
        min="0"
        value={formItem.amount}
        onChange={(e) => handleInputChange(e)}
      />
      <br />
      <label htmlFor="unit">Unit:</label>
      <br />
      <input
        type="string"
        id="unit"
        className="input_field"
        value={formItem.unit}
        onChange={(e) => handleInputChange(e)}
        placeholder="Unit of the item (gramm, kg, liter, ...)"
      />
      <br />
      <div className="div_btn_add_item">
        <button className="btn_add_item" onClick={() => props.onAdd(formItem)}>
          Add item! 🛒
        </button>
      </div>
    </div>
  );
};

export default ItemForm;
