import Item from "../models/item";

class ShoppinglistApiService {
  apiUrl: string;

  constructor() {
    this.apiUrl = "https://json.ddnss.eu/shoppinglistitems";
  }

  // GET all items
  async getItems(filterOption: string = "all"): Promise<Item[]> {
    let filter = "";
    switch (filterOption) {
      case "all":
        break;
      case "yay":
        filter += "?done=true";
        break;
      case "nay":
        filter += "?done=false";
        break;
    }

    const response = await fetch(`${this.apiUrl}/${filter}`);
    const data = await response.json();
    return data;
  }

  //   // GET ONE item
  async getItem(id: number): Promise<Item> {
    const response = await fetch(`${this.apiUrl}/${id}`);
    const data = await response.json();
    return data;
  }

  //   // POST item
  async createItem(item: Item): Promise<Item> {
    const response = await fetch(`${this.apiUrl}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(item),
    });
    const data = await response.json();
    return data;
  }

  //   // UPDATE item
  async updateItem(item: Item): Promise<Item> {
    if (!item.id) {
      throw "No ID specified for item!";
    }

    const response = await fetch(`${this.apiUrl}/${item.id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(item),
    });
    const data = await response.json();
    return data;
  }

  //   // DELETE item
  async deleteItem(id: number): Promise<void> {
    await fetch(`${this.apiUrl}/${id}`, {
      method: "DELETE",
    });
  }
}

export default ShoppinglistApiService;
