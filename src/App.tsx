import "./App.css";
import Header from "./components/header";
import ItemList from "./components/item-list";

function App() {
  return (
    <div className="div_app">
      <div className="div_header_title">
        <Header headerText="Shopping List" />
        <hr />
      </div>

      <ItemList />
    </div>
  );
}

export default App;
